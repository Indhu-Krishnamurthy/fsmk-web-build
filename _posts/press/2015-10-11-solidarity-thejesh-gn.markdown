---
layout: post
title:  "Solidarity with Thejesh GN"
date:   2015-10-06 18:28:01 +0530
category: press
---







Free Software Movement Karnataka(FSMK) expresses its deep anguish at the turn of events surrounding fellow hacktivst Thejesh GN, who exposed Airtel and its shady means of pursuing profits. While we do not condone the act of publishing proprietary code on Github, weheartily congratulate and salute the spirit of investigation and greater good in what Thejeshwas trying to do.

The issue brings face-to-face with the unfortunate combination of three great dangers Indian mobile subscribers and netizens today face viz.,

1) Blatant violation of the net neutrality principle

2) Unabashed breach of consumer's privacy

3) Threat of unregulated mass surveillance by foreign technology companies

The Snowden episode has given us all strong enough warnings about the role played by technology companies who, in the name of advertisement revenue, indulge in unethical practices of profiling consumers by tracking their usage and behaviour. Such centralized data collection has made it easier for the operations of the National Security Agency(NSA). We do not believe Airtel can devolve itself from this issue as its Israeli vendor, Flash Network Ltd., has clearly mentioned in its letter to Thejesh, that the code to be injected into subscribers browserswas created and owned by Airtel. We do not agree with Airtel's excuse that the said program is only to keep track of the user's data usage. We doubt if its even the right technical solution for that problem.

We demand Airtel and all other telecom companies to disclose to its current and potential users all such practices that might affect their data privacy. We are deeply concerned about interception and modification of content by ISPs, thus violating Net Neutrality in more than one way.

We also demand the Ministry of Communications and IT to take stringent action on such practices and explain to the citizens of India how they can assure that our communications are not subjected to unfair mass surveillance and profiling by foreign or domestic technology companies.

We also call upon all netizens, hackers and fellow FOSS enthusiasts to watch out for such practices by other telecom operators and expose them carefully and effectively.

 

You can find printable version of press note here

