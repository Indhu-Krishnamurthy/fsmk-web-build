—-
layout: post
title:  "Full time and Internship positions at FSMK (revised announcement)"
date:   2016-02-22 17:42 +0530
category: organisation
discourse_topic_id: 
—-

We've previously announced regarding FSMK hiring interns and full timers.
We've come up with better roles and responsibilities to make things easier
for you and us!

Here you go:

Free Software Movement Karnataka(FSMK) is offering internships for students
and graduates who would like to contribute to the free software movement
through a range of tasks. These are a combination of both part-time and
full-time positions. The positions are divided into technical and
non-technical positions and applicants are free to choose from a subset
though one strong area is a minimum requirement.

PROFILE FOR INTERNS AT FSMK

= Technical =
==System/Network admins for FSMK (No. of positions - 2)==
1. fsmk.org and all its sub-domains (for example, camp.fsmk.org) to be
managed.

    setup beta environment for everything.
    setup backup and restore policy.
    migrate to https.
    All content should be version-ed
        Manage the mail server for FSMK.
    Accounts creation.
    Backup of the existing mails.
        Procurement of additional hardware.
    requirement gathering.
    Budget calculation.
    procurement
    Setup the infrastructure.
        Update the software's.
    Identify any security vulnerabilities.
    Update the softwares to the latest patches.
    Test the patches in Beta before implementing in production.
        Automation of regular operations tasks on the existing and new servers.
        Setup GLUG pages in FSMK website.
        User management in FSMK systems.
        Setup the summer camp infrastructure and servers.
        Audit the expenses on existing computing infrastructures including FSMK
        website and mail servers. Publish the details to the FSMK EC and Core.
        Document all DevOps related activities under FSMK.

== Spoken tutorials (No. of Positions - 2) ==

    Translate Spoken Tutorials to Kannada
    Conduct workshops for faculty members of engineering colleges
    Provide technical support for lab manual implementation
    Improve the existing android app(FSMK lab manual) and support
    infrastructure

= Non technical =

== Outreach intern (No. of positions - 2) ==

    Help devise, conceptualise, implement and support technical and
    community events in Karnataka
    Reach out to people of different walks of life and help them understand
    FS and how FSMK is contributing to the society.
    Reach out to people on social media and help get more resources to
    conduct sessions (technical/non-technical)
    Support the growth of GNU/Linux User Groups (GLUG) related activities in
    different colleges all over Karnataka by periodic engagement with GLUG
    members
    Help run fund raisers

== Communication intern (No. of positions - 1) ==

    Assist with public relations(PR) and communications of the organisation
    through social media and other online-offline channels
    Help develop FSMK promotional pamphlets, digital media and other
    outreach material primarily in English with Kannada and Hindi as secondary
    languages
    Help run fund raisers
    Help prepare reports of activities

== Visual Design (No. of Positions - 1) ==
1. Design FSMK promotional material, digital media and other outreach
material primarily in English with Kannada and Hindi as secondary languages

== Policy Interns (No. of Positions - 2) ==
1. Should be able to engage with policy issues related to free software and
the movement(example, Section 66A, net neutrality, patents, etc) and draft
positional documents

= Requirements =

    Understanding of the FSMK community and the free software philosophy
    Interest to work in a not-for-profit environment
    Leadership skills especially in working with voluntary groups
    Basic coding skills and ability to learn new skills to support projects
    Track record in writing, web editing and social media
    Interest in supporting volunteers and volunteer-run projects
    Ability to work effectively with a team of volunteers
    Commitment for at least three to six months. Project-based internships
    available.

= Perks =

    Ability to negotiate projects and tasks
    Contribute from outside Bangalore (depends on statement of purpose)
    Nominal stipend (on a case-by-case basis).
    Experience certificate will be provided at the end of the internship.

= Apply =

Prepare a statement of purpose within 250 words stating how you plan to
contribute to FSMK. Include your vision statement for the organisation and
how you plan to implement it. Send it via email to admin AT fsmk DOT org (
admin@fsmk.org)

NOTE: Use only free document formats. Non-free formats will be discarded
without notice.
